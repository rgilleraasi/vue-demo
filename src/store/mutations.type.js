export const SET_USERS = 'setUsers'
export const SET_USER = 'setUser'
export const ADD_USER = 'addUser'
export const UPDATE_USER_IN_LIST = 'updateUserInList'
