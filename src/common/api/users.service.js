import ApiService from './api.service'

export const UserService = {

  query (params) {
    return ApiService
      .query(
        'persons', { params: params }
      )
  },

  get (slug) {
    return ApiService.get('persons', slug)
  },

  create (params) {
    return ApiService.post('persons', {user: params})
  },

  destroy (slug) {
    return ApiService.delete(`persons/${slug}`)
  },

  update (slug, params) {
    return ApiService.update('persons', slug, params)
  }
}
