import { UserService } from '../common/api/users.service'
import {
  FETCH_USERS,
  FETCH_USER,
  ADD_USER,
  DELETE_USER,
  UPDATE_USER
} from './actions.type'
import {
  SET_USERS,
  SET_USER,
  UPDATE_USER_IN_LIST
} from './mutations.type'

const state = {
  users: [],
  user: {}
}

const actions = {
  [FETCH_USERS] (context) {
    return UserService.query()
      .then(({ data }) => {
        context.commit(SET_USERS, data)
        return data
      })
  },
  [FETCH_USER] (context, slug) {
    return UserService.get(slug)
      .then(({ data }) => {
        context.commit(SET_USER, data)
        return data
      })
  },
  [ADD_USER] (context, payload) {
    return UserService.create(payload)
      .then(({ data }) => {
        context.commit(SET_USERS, data)
        return data
      })
  },
  [UPDATE_USER] (context, slug, payload) {
    return UserService.update(slug, context.getters.user)
      .then(({ data }) => {
        context.commit(SET_USERS, data)
        return data
      })
  },
  [DELETE_USER] (context, slug) {
    return UserService.destroy(slug)
      .then(({ data }) => {
        context.commit(
          UPDATE_USER_IN_LIST,
          data.user,
          { root: true }
        )
      })
  }
}

const mutations = {
  [SET_USERS] (state, users) {
    state.users = users
  },
  [UPDATE_USER_IN_LIST] (state, user) {
    state.users = state.users.$remove(user)
  },
  [SET_USER] (state, data) {
    state.user = data[0]
  }
}

const getters = {
  users (state) {
    return state.users
  },

  user (state) {
    return state.user
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
