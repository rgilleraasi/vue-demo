import Vue from 'vue'
import Router from 'vue-router'

// import FranchiseEngagement from '@/views/FranchiseEngagement'
import FranchiseApplication from '@/views/FranchiseApplication'
import AccountInformation from '@/views/AccountInformation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/views/auth/Login'),
      meta: {
        plainLayout: true
      }
    },
    {
      path: '/forgot-password',
      name: 'ForgotPassword',
      component: () => import('@/views/auth/ForgotPassword'),
      meta: {
        plainLayout: true
      }
    },
    {
      path: '/register',
      name: 'Register',
      component: () => import('@/views/auth/Register'),
      meta: {
        plainLayout: true
      }
    },
    // {
    //   path: '/',
    //   name: 'FranchiseEngagement',
    //   component: FranchiseEngagement
    // },
    {
      path: '/franchise-application',
      name: 'FranchiseApplication',
      component: FranchiseApplication
    },
    {
      path: '/account-information',
      name: 'AccountInformation',
      component: AccountInformation
    },
    {
      path: '/',
      name: 'UserInformation',
      component: () => import('@/views/UserInformation')
    },
    {
      path: '/users',
      name: 'UserInformation',
      component: () => import('@/views/UserInformation')
    },
    {
      path: '/add-user',
      name: 'UserInformationAdd',
      component: () => import('@/views/UserInformationAdd')
    },
    {
      path: '/edit-user/:id',
      name: 'UserInformationEdit',
      component: () => import('@/views/UserInformationEdit')
    }
  ]
})
